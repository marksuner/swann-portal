<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Swann Portal </title>
    <link href="/css/admin.css" rel="stylesheet">
    <link href="/css/admin-custom.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/css/bootstrap-tagsinput.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <meta id="token" data-content="{{csrf_token()}}">
    <link rel="icon"
          type="image/png"
          href="/favicon.png">
    <script src="https://cdn.tiny.cloud/1/vu6gr5c3bi5jatoty0lgmi7fu78xszr7vxx92gqrq0v0gtmn/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea#target_tinymce',
            skin: "oxide",
            icons: 'material' ,
            min_height: 500,
            preview_styles: 'font-size color',
            resize: 'both',
            plugins: 'link code autolink lists table',
            toolbar: 'undo redo | styleselect | forecolor | bold italic | bullist numlist | alignleft aligncenter alignright alignjustify | outdent indent | link | code table',
            toolbar_mode: 'floating',
            tinycomments_mode: 'embedded',
        });
    </script>
    @yield('header')
</head>