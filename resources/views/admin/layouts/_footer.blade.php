
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="/js/jquery.min.js"></script>
    {{-- <script src="https://code.jquery.com/jquery-2.2.4.js"></script> --}}

    <!-- Bootstrap Core JavaScript -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap-tagsinput.min.js"></script>
    <script>
        $(document).ready( function() {
            $('.btn-delete').on('click', function(e) {
                if(confirm('Deleting data cannot be undone. Are you sure about this? ')) {
                    return true;
                }

                e.preventDefault();
            });

            // $('form').on('submit', function() {
            //     $($(this).find(':submit').attr('disabled', 'disabled');
            // });
            $('.tags').tagsinput({
              cancelConfirmKeysOnEmpty: false
            }); 
        });
    </script>
    @yield('footer')
</body>

</html>
