@extends('admin.layouts.content')

@section('Page__Description')
    Albums
@stop

@section('Content')
    <div id="app">
        <div class="pull-right">
            <a class="btn btn-primary" data-toggle="modal" href='#Modal__AlbumForm'>New Screenshots</a>
        </div>

        <ol class="breadcrumb">
            <li>
                <a  
                    class="mouse__pointer"
                    @click="get()">Albums</a>
            </li>
            <li v-for="(index, parent) in parents">
                <a  
                    class="mouse__pointer"
                    @click="onBreadCrumbClick(index, parent)"
                >@{{ parent.name }}</a>
            </li>
        </ol>

        <div 
            class="progress"
            v-show="progress > 0"
        >
          <div class="progress-bar progress-bar-info progress-bar-striped" 
                role="progressbar" 
                v-bind:style="{width: progressStyle}"
            >
            <span class="sr-only"></span>
          </div>
        </div>
        <div 
            class="panel panel-primary"
            v-show="albums.length > 0"
        >
            <div class="panel-heading">
                <h3 class="panel-title">Albums</h3>
            </div>
            <div class="panel-body">
                <div class="row" v-for="chunks in albums | chunk 6">
                  <div class="col-xs-6 col-md-2" v-for="album in chunks">
                    <div class="thumbnail thumbnail__album" @click="onAlbumClick(album)">
                      <img src="/img/folder.png">
                      <div class="caption">
                          <h5>@{{ album.name }}</h5>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="panel panel-info"
            v-show="parents.length > 0"
        >
            <div class="panel-heading">
                <h3 class="panel-title">
                    Screenshots

                    <a href="#" title="Add Screenshot" data-toggle="tooltip" class="btn-action" @click="onShowModalUpdate()">
                        <i class="glyphicon glyphicon-plus"></i>
                    </a>
                </h3>
            </div>
            <div class="panel-body">
                {{-- <div class="row" v-for="chunkScreenshot in screenshots | chunk 6">
                  <div class="col-xs-6 col-md-2" v-for="screenshot in chunkScreenshot">
                    <div class="thumbnail thumbnail__album">
                        <button type="button" class="close" aria-hidden="true" @click="onDeleteScreenshot(screenshot.id)">&times;</button>
                      <img v-bind:src="screenshot.path">
                    </div>
                  </div>
                </div> --}}
                <ul class="Screenshots__List" id="Screenshots__List">
                    <li class="Screenshots__List-item"
                        v-for="screenshot in screenshots | orderBy 'order' "
                    >
                        <button type="button" class="close Screenshots__Button-close" 
                                aria-hidden="true" 
                                @click="onDeleteScreenshot(screenshot.id)"
                                title="Remove" data-toggle="tooltip"
                        >&times;</button>
                        <img v-bind:src="screenshot.path" style="max-width: 150px">
                    </li>
                </ul>
            </div>
    </div>

@stop

@section('header')
    <link rel="stylesheet" href="/css/select2.css">
    <link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
    <style>
        .Screenshots__List {
        }

        .Screenshots__List-item {
            display: inline-block;
            /*padding: 5px;*/
            border: 1px solid #A4A4A4;
            margin-right: 5px;
            margin-bottom: 5px;
            cursor: move;
            cursor: -webkit-grabbing;
        }

        .Screenshots__List-item > img {
            width: 100%;
        }

        .Screenshots__Button-close {
            float: none;
            position: absolute;
            color: red;
            opacity: 1;
        }

        .Screenshots__Button-close:hover {
            border: 1px solid red;
        }

        .Screenshots__List-item, .Screenshots__Button-close  {
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
        }
    </style>
@stop

@section('footer')
    <div class="modal fade" id="Modal__AlbumForm">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Screenshots Form</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" method="POST" id="Modal__AlbumForm--Real">
                        {!! csrf_field() !!}
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label">Album</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="album" value="" v-model="form.name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" >Parent Album</label>

                            <div class="col-md-6">
                                <select name="parent" class="form-control" v-model="form.parent">
                                    <option value="">
                                        -- No Parent Album --
                                    </option>
                                    @foreach($parents as $parent)
                                        <option value="{{$parent->id}}">
                                            {{ $parent->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Product</label>

                            <div class="col-md-6">
                                <select name="product[]" multiple class="form-control select2"></select>
                            </div>
                        </div>
                        <div class="form-group hide">
                            <label class="col-md-4 control-label">Product</label>

                            <div class="col-md-6">
                                <select name="images[]" multiple class="form-control">
                                    <option 
                                            v-for="(key, image) in uploadedImage" 
                                            v-bind:value="image.id"
                                            selected 
                                    >
                                        @{{ image.id }}
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div id="ImageDropzone" class="dropzone"></div>
                        <div class="row" v-for="images in uploadedImage | chunk 4">
                          <div class="col-sm-6 col-md-3" v-for="(key, image) in images">
                            <div class="thumbnail">
                                <button type="button" class="close" aria-hidden="true" @click="onRemoveUploadedImage(key)">&times;</button>
                                <img v-bind:src="image.path">
                            </div>
                          </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" @click="onFormSubmit()">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <script src="/js/select2.js"></script>
    <script src="/js/procedures.js"></script>

    <script src="/js/dropzone.min.js"></script>

    <script src="/js/vue.js"></script>
    <script src="/js/vue-chunk.js"></script>
    <script src="/js/Sortable.min.js"></script>
    <script src="/js/vue-sortable.js"></script>

    <script>
        ( function() {
            Dropzone.autoDiscover = false;
            
            new Vue({
              el: '#app',
              data: {
                albums: [],
                screenshots: [],
                parents: [],
                active: 0,
                progress: 0,
                progressStyle: '',
                uploadedImage: [],
                myDropzone: null,
                form: {
                    name : '',
                    parent : '',
                    products : []
                }
              },
              ready: function() {
                this.myDropzone = new Dropzone("div#ImageDropzone",{
                    url: "/api/files",
                    headers : {
                        'X-CSRF-TOKEN' : $('#token').data('content')
                    },
                    addRemoveLinks: true,
                    acceptedFiles: 'image/*',
                    paramName: 'image',
                    success: function(file, response) {
                        this.uploadedImage.push(response);
                    }.bind(this),
                    complete: function(file) {
                        this.myDropzone.removeFile(file);
                    }.bind(this)
                });
                this.get();
              },
              methods: {
                get: function(id) {
                    var path = '/admin/albums/all';
                    var _self = this;
                    _self.progress = 10;
                    _self.progressStyle = _self.progress + '%';
                    var progressBar = setInterval( function() {
                        if (_self.progress <= 80) {
                            _self.progressStyle = _self.progress + '%';
                            _self.progress += 10;
                        }
                    }.bind(this), 100);
                    if (typeof id !== 'undefined') {
                        path += "/" + id;
                        _self.active = id;
                    } else {
                        _self.active = 0;
                        _self.parents = [];
                    }

                    $.get(path)
                        .success( function(response) {
                            _self.albums = response.categories;
                            _self.screenshots = response.screenshots;
                            Sortable.create(document.getElementById('Screenshots__List'), {
                                animation: 80,
                                onUpdate: function(evt) {
                                    console.log('dropped (Sortable)');
                                    _self.onReorder(evt.oldIndex, evt.newIndex);
                                }
                            });
                            clearInterval(progressBar);
                            _self.progressStyle = '100%';
                            setTimeout( function(){
                                _self.progress = 0;
                            }, 500)
                        });
                },
                onAlbumClick: function(album) {
                    this.get(album.id);
                    this.parents.push(album);
                },
                onBreadCrumbClick: function(index, album) {

                    if (this.active == album.id) {
                        return null;
                    }

                    if (index == 0) {
                        this.parents = [];
                    }

                    this.onAlbumClick(album);
                },
                onRemoveUploadedImage: function(index) {
                    this.uploadedImage.splice(index, 1);
                },
                onFormSubmit: function() {
                    var data = $('#Modal__AlbumForm--Real').serialize();
                    $.ajax({
                        method: 'post',
                        url: '/api/files/persists',
                        headers : {
                            'X-CSRF-TOKEN' : $('#token').data('content')
                        },
                        data: data
                    }).success( function(response) {
                        this.uploadedImage = [];
                        this.form = {
                            name: '',
                            parent: ''
                        };
                        $('.select2').select2("val", "");
                        alert('Added new Images');
                    }.bind(this));
                },
                onDeleteScreenshot: function(id) {
                    if( !confirm("are your sure? ")) {
                        return;
                    }
                    
                    $.ajax({
                        method: 'post',
                        'url': '/api/screenshots/' + id,
                        headers : {
                            'X-CSRF-TOKEN' : $('#token').data('content')
                        },
                    }).success( function(response) {
                        var count = this.parents.length - 1;

                        this.get(this.parents[count].id);

                    }.bind(this));
                },
                onShowModalUpdate: function() {
                    this.form.name = this.parents[this.parents.length - 1].name;
                    this.form.parent = this.parents[this.parents.length - 1].parent_id;
                    $('#Modal__AlbumForm').modal('show');
                },
                onReorder: function(oldIndex, newIndex) {
                  // move the item in the underlying array
                  this.screenshots.splice(newIndex, 0, this.screenshots.splice(oldIndex, 1)[0]);
                  // update order property based on position in array
                  this.screenshots.forEach(function(item, index){
                    item.order = index;
                  });

                  $.ajax({
                    method: 'POST',
                    url: '/admin/albums/update-order',
                    headers : {
                        'X-CSRF-TOKEN' : $('#token').data('content')
                    },
                    data : { screenshots : this.screenshots }
                  })
                    .success( function(response) {
                        console.log(response);
                    })
                }
              }
            });
            $('[data-toggle="tooltip"]').tooltip();

        }());
    </script>
@stop