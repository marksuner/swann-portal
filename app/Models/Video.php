<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'title', 'description', 'name', 'extension', 'converted', 'featured'
    ];

    public function keyword()
    {
        return $this->morphOne(Keyword::class, 'keywordable');
    }

    public function getMP4()
    {
        $value = config('swannportal.path.videos') . $this->name . '.' . 'mp4';

        // return 'https://swann-portal.s3.ap-southeast-1.amazonaws.com/' . $value;
        return 'https://swann-portal-2.s3.ap-southeast-1.amazonaws.com/' . $value;

        // $disk = \Storage::disk('s3');

        // if ($disk->exists($value)) {
        //     return $disk->url($value);
        // }

        // return url($value);
    }

    public function getOGG()
    {
        $value = config('swannportal.path.videos') . $this->name . '.' . 'ogg';
        // return 'https://swann-portal.s3.ap-southeast-1.amazonaws.com/' . $value;
        return 'https://swann-portal-2.s3.ap-southeast-1.amazonaws.com/' . $value;


        // $disk = \Storage::disk('s3');

        // if ($disk->exists($value)) {
        //     return $disk->url($value);
        // }

        // return url($value);
    }

    public function news()
    {
        return $this->hasOne(News::class);
    }
}
