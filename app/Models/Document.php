<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'name', 'extension'
    ];

    public function getDocument()
    {
        $value = config('swannportal.path.documents') . $this->name . '.' . $this->extension;
        // return 'https://swann-portal.s3.ap-southeast-1.amazonaws.com/' . $value;
        return 'https://swann-portal-2.s3.ap-southeast-1.amazonaws.com/' . $value;
        // $disk = \Storage::disk('s3');

        // if ($disk->exists($value)) {
        //     return $disk->url($value);
        // }

        // return url( config('swannportal.path.documents') . $this->name . '.' . $this->extension);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

}
