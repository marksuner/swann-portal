<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'name', 'extension'
    ];

    public function slider()
    {
        return $this->hasOne(Slider::class);
    }

    public function getImage()
    {
        $value = config('swannportal.path.images') . $this->name . '.' . $this->extension;

        // return 'https://swann-portal.s3.ap-southeast-1.amazonaws.com/' . $value;
        return 'https://swann-portal-2.s3.ap-southeast-1.amazonaws.com/' . $value;

        // $disk = \Storage::disk('s3');

        // if ($disk->exists($value)) {
        //     return $disk->url($value);
        // }

        // return url( config('swannportal.path.images') . $this->name . '.' . $this->extension);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
