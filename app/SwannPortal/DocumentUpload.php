<?php
namespace App\SwannPortal;

use App\Models\Document;
use Illuminate\Http\Request;

class DocumentUpload
{
    protected $id;
    protected $request;
    protected $documents;

    public $destination = null;

    function __construct(Request $request, Document $documents, $id = false)
    {
        $this->id = $id;
        $this->request = $request;
        $this->documents = $documents;

        $this->destination = config('swannportal.path.documents');
    }

    public function handle()
    {
        $disk = \Storage::disk('s3');

        if(!$this->request->hasFile('document'))
        {
            if ($this->id) {
                $document = $this->documents->findOrFail($this->id);

                return $document;
            }

            return null;
        }

        $extension = $this->request->file('document')->getClientOriginalExtension();
        $fileName = str_random(40);

        $disk->put($this->destination . $fileName . '.' . $extension, file_get_contents($this->request->file('document')->getRealPath()), 'public');

        if($this->id) {
            $document = $this->documents->findOrFail($this->id);

            $disk->delete(config('swannportal.path.documents') . $document->name . '.' . $document->extension);

            $document->update([
                'name'        => $fileName,
                'extension'   => $extension
            ]);

        } else {
            $document = $this->documents->create([
                'name'        => $fileName,
                'extension'   => $extension
            ]);
        }

        return $document;
    }
}