<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Models\Screenshot;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ScreenshotsController extends Controller
{
    protected $screenshots;

    public function __construct(Screenshot $screenshots)
    {
        $this->screenshots = $screenshots;
    }

    public function destroy($id)
    {
        $this->screenshots->findOrFail($id)->delete();

        return [
            'status' => 1
        ];
    }
}
