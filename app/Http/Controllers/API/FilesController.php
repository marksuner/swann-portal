<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Models\Photo;
use App\Models\Screenshot;
use App\Models\ScreenshotCategory;

use App\Http\Requests;
use App\SwannPortal\ImageUpload;
use App\Http\Controllers\Controller;

class FilesController extends Controller
{
    public function __construct() 
    {

    }

    public function store(Request $request, Photo $photos)
    {
        $photo = (new ImageUpload($request, $photos))->handle();

        return [
            'id'   => $photo->id,
            'path' => $photo->getImage()
        ];
    }

    public function persists(
            Request $request,
            ScreenshotCategory $categories,
            Screenshot $screenshots
    )
    {

        $category = $categories->firstOrCreate([
            'name'      => $request->input('album'),
            'parent_id' => $request->input('parent')?: 0
        ]);

        foreach($request->input('images') as $image):
            $screenshot = $screenshots->create([
                'name'        => ' ',
                'photo_id'    => $image,
                'category_id' => $category->id,
                'order'       => 0
            ]);
            $screenshot->products()->sync($request->input('product'));
        endforeach;


        return ['status' => 1];
    }
}
