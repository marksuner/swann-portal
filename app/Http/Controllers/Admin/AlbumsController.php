<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Screenshot;
use App\Models\ScreenshotCategory;
use App\Http\Controllers\Controller;

class AlbumsController extends Controller
{
    protected $categories;

    public function __construct(ScreenshotCategory $categories)
    {
        $this->categories = $categories;
    }

    public function getIndex()
    {
        $parents = $this->categories->where('parent_id', 0)->get();

        return view('admin.albums.index', compact('parents'));
    }

    public function getAll($id = null)
    {   
        $categories = $this->categories->with('screenshots');
        $screenshots = [];

        if ($id) {
            $active = $this->categories->with(['screenshots', 'screenshots.photo'])->findOrFail($id);
            $screenshots = [];

            foreach($active->screenshots as $screenshot):
                $screenshots[] = [
                    'id'       => $screenshot->id,
                    'photo_id' => $screenshot->photo_id,
                    'order'    => $screenshot->order,
                    'path'     => $screenshot->photo->getImage()
                ];
            endforeach;

            $categories = $categories->where('parent_id', $id)->get();

        } else {
            $categories = $categories->where('parent_id', 0)->get();
        }

        return [
            'categories' => $categories,
            'screenshots'=> $screenshots
        ];
    }

    public function postUpdateOrder(Request $request, Screenshot $screenshots)
    {
        $lists = $request->input('screenshots');

        foreach($lists as $key => $list):
            $screenshots->findOrFail($list['id'])->update([
                'order' => $key
            ]);
        endforeach;

        return [
            'status' => 1
        ];
    }
}
