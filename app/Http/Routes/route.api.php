<?php

Route::group(['namespace' => 'API', 'prefix' => 'api'], function() {
    Route::post('files', 'FilesController@store');
    Route::post('files/persists', 'FilesController@persists');

    Route::post('screenshots/{id}', 'ScreenshotsController@destroy');
    Route::resource('products', 'ProductsController', ['only' => 'index']);
    Route::resource('lmi-sessions', 'LmiSessionsController', ['only' => ['index', 'store']]);
    Route::controller('agreements', 'AgreementsController');
    Route::post('feedbacks', 'FeedbacksController@store');
    // Route::resource('decisions', 'DecisionsController',
    //         ['only' => ['index', 'store', 'show']]
    // );
});