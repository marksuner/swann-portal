<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class SyncIPData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:ip:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload database data with a value of the IP given';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $search = [
            '192.168.0.5',
            '180.232.71.230',
            '54.255.72.78',
            '13.229.102.83',
        ];

        $replace = '54.254.128.140';

        // get columns
        // $columns = DB::table('information_schema.columns')
        //             ->select(['data_type'])
        //             ->select(['column_name', 'data_type', 'table_name'])
        //             ->where('TABLE_SCHEMA', 'swann_portal')
        //             ->whereNotIn('DATA_TYPE', [
        //                 'timestamp', 'int', 'float', 'double', 'datetime', 'date', 'tinyint', 'bigint'
        //             ])
        //             ->orderBy('TABLE_NAME', 'asc')
        //             ->get();
        $columns = [
            [
                'table'  => 'products',
                'column' => 'description'
            ],
            [
                'table'  => 'questions',
                'column' => 'answer'
            ],
            [
                'table' => 'footers',
                'column' => 'content'
            ]
        ];

        foreach($columns as $column) {
            $selectQuery = DB::table($column['table'])->select([$column['column'], 'id']);

            foreach($search as $key => $value) {
                // if the search is just the start
                if ($key === 0) {
                    $selectQuery = $selectQuery->where($column['column'], 'like', '%' . $value . '%');
                } else {
                    $selectQuery = $selectQuery->orWhere($column['column'], 'like', '%' . $value . '%');
                }
            }

            if ($selectQuery->count() === 0) {
                continue;
            }

            $items = $selectQuery->get();

            foreach($items as $item) {
                // replace the value
                $value = $item->{$column['column']};
                foreach($search as $s) {
                    $value = str_replace($s, $replace, $value);
                }

                // actual update
                DB::table($column['table'])->where('id', $item->id)->update([
                    $column['column'] => $value,
                ]);
            }
        }

    }
}
